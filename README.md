> **⚠️ Warning:** This is an old version of Tadah! Users are encouraged to migrate to the new version [here](https://git.ecdf.ed.ac.uk/tadah).


Ta-dah!
=======

The Ta-dah! is a modular and fast machine learning library specifically designed for the inter-atomic potential development in mind.
The library is written in modern C++ with the intention to provide easy to use, modular and extensible state-of-the-art toolkit.


Documentation
=============

Up to date documentation and installation instruction can be found at
    
https://ta-dah.readthedocs.io
