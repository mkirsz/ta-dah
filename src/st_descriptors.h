#ifndef ST_DESCRIPTORS_H
#define ST_DESCRIPTORS_H

#include <vector>
#include "libs/Eigen/Dense"
#include "libs/Eigen/src/Core/Array.h"
#include "libs/Eigen/src/Core/util/Constants.h"
#include "structure.h"
#include "config.h"

// Temp solution to patch lammps stack smashing
// when Eigen::VectorXd is used as a container - rhoi_type
// lammps has a tendency to crash occasionally
// Only observed so far when hpo is used
// - It seem to be fine for aeds and fd
// - The crash happens during vector resizing
// - could be memory allocation incompatibility
class my_vec {
    private:
        std::vector<double> v;
    public:
        void setZero() {
            std::fill(v.begin(), v.end(), 0);
        }
        double &operator()(const size_t i) {
            return v[i];
        }
        void resize(size_t n) {
            v.resize(n);
        }
        size_t size() const {
            return v.size();
        }
};


/** \brief Container for a structure descriptors.
 *
 * Contains:
 * - Atomic Energy Descriptors
 * - Force Descriptors (Optional)
 * - Stress Descriptors (Optional)
 *
 * To fully initialise this object it needs to know:
 * - number of atoms in a structure (from Structure)
 * - number of nn for every atom (from Structure)
 * - dimension of the descriptor vector (Config)
 * - whether force and stress is being calculated (from Config)
 *
 *   \note
 *       Required Config keys:
 *       \ref FORCE \ref STRESS.
 *       \ref INTERNAL_KEY \ref DSIZE.
 *
 */
struct StDescriptors {

    /** Atomic Energy Descriptor (AED) type
     *
     * Dimensions:
     * [descriptor size]
     *
     */
    //typedef Eigen::Matrix<double, Eigen::Dynamic, 1> aed_type;
    //typedef Eigen::VectorXd aed_type;
    //typedef std::vector<Eigen::VectorXd> aeds_type;

    typedef Eigen::VectorXd aed_type;
    typedef Eigen::Ref<Eigen::VectorXd> aed_rtype;
    typedef Eigen::Ref<const Eigen::VectorXd> aed_rctype;
    typedef Eigen::MatrixXd aeds_type;  // [s,natoms]

    /** Force Descriptor (FD) type
     *
     * Dimensions:
     * [descriptor size, 3]
     *
     */
    typedef Eigen::Matrix<double, Eigen::Dynamic, 3> fd_type;

    /** Stress Descriptor (SD) type
     *
     * Dimensions:
     * [descriptor size,6]
     *
     */
    typedef Eigen::Matrix<double, Eigen::Dynamic, 6> sd_type;   // sigma_ij order: 11,12,13,22,23,33

    /** Density array to be used by many-body descriptors... TODO
     *
     * Dimensions:
     * [atom i][n]
     *
     * where n is specific to given descriptor
     *
     */
    //typedef Eigen::VectorXd rhoi_type;
    //typedef Eigen::Ref<Eigen::VectorXd> rhoi_type;
    //typedef Eigen::Block<Eigen::Matrix<double, -1, -1>, -1, 1, true> rhoi_type;
    //typedef my_vec rhoi_type;
    typedef Eigen::Ref<Eigen::VectorXd> rho_rtype;
    //typedef std::vector<rhoi_type> rhos_type;
    typedef Eigen::MatrixXd rhos_type;

    /** This constructor fully initialise this object
     *
     * Requires:
     * - Structure st to have NN calculated
     * - Config c to contain keys: \ref DSIZE, \ref FORCE, \ref STRESS
     */
    StDescriptors(const Structure &s, const Config &c);

    /** Default constructor. Object is left uninitialised */
    StDescriptors();

    /** AED for all atoms
     *
     * This object will always be initialised given
     * provided Structure contains atoms.
     *
     * Dimensions:
     * [number of atoms, descriptor size]
     */
    aeds_type aeds;

    /** FD for all atoms
     *
     * Dimensions:
     * [number of atoms, number of atom NN, descriptor size, 3]
     */
    std::vector<std::vector<fd_type>> fd;

    /** SD for a Structure
     *
     * Dimensions:
     * [descriptor size, 6]
     */
    sd_type sd;

    /** True if descriptors are normalised */
    bool normalised=false;

    rhos_type rhos;

    aed_rtype get_aed(const size_t i);
    const aed_rctype get_aed(const size_t i) const;
    rho_rtype get_rho(const size_t i);

    size_t naed() const;
    size_t dim() const;

};
#endif
