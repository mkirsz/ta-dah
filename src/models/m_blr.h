#ifndef M_BLR_H
#define M_BLR_H

#include "m_base.h"
#include "../cutoffs/cutoffs.h"
#include "linear_regressor.h"
#include "../config.h"
#include "../descriptors_calc.h"

#include "../descriptors/d_all.h"
#include "../kernels/kern_all.h"
#include "design_matrix.h"
#include "../nn_finder.h"
#include <limits>
#include <stdexcept>
#include <type_traits>
#include "basis.h"
#include "../basis_functions/bf_all.h"
#include "../libs/Eigen/Eigenvalues"
#include "../utils/registry.h"

/** Bayesian Linear Regression
 *
 * Model supported training modes:
 *
 *  - LINEAR:     Ordinary Least Squares or Ridge Regression (Regularised Least Squares)
 *
 *  - NONLINEAR:  Linear in parameters but nonlinear in an input space.
 *                Each row of a matrix \f$\Phi\f$ is a vector-valued function
 *                of the original input descriptor \f$ \mathbf{\phi(x)}^T \f$
 *                e.g. BF_Polynomial2:
 *                (1, x_1, x_2) -> (1, x_1, x_2, x_1x_2, x_1^2, x_2^2)
 *
 * Basis functions are selected at random. Number of basis functions is
 * controled by \ref SBASIS key in a Config file.
 *
 * **Prediction**:
 *
 * \f[
 * y(\mathbf{x}, \mathbf{w}) = \sum_{j=0}^{M-1} w_j \phi_j(\mathbf(x))
 * \f]
 *
 * where M is a number of parameters, **w** are machine learned weights
 * and **x** is a vector of input variables.

 *
 * **Training**:
 *
 * \f[
 * \mathbf{w} = (\lambda \mathbf{I} + \mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y}
 * \f]
 *
 * for \f$\lambda=0\f$ this reduces to Ordinary Last Squares (OLS) aka
 * Linear Least Squares with the normal equation:
 *
 * \f[
 * \mathbf{w} = (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y}
 * \f]
 *
 * This model is always a linear function of its parameters \f$w_i\f$.
 *
 * When used with linear basis function \f$\phi\f$ (here BF_Linear)
 * it is also linear wrt to the input variables (here descriptors).
 *
 * When nonlinear basis function \f$\phi\f$ is used
 * the function \f$y(\mathbf{x}, \mathbf{w})\f$ becomes nonlinear wrt to **x**
 * but still linear wrt to **w**.
 *
 * **Optional Config keys**:
 *
 *  - \ref LAMBDA = 0  -> use OLS
 *  - \ref LAMBDA > 0  -> manually set to given value
 *  - \ref LAMBDA = -1 -> use evidence approximation to find \f$\lambda\f$
 *  - \ref SBASIS N    -> Use N basis functions when nonlinear BF is used
 *
 * @tparam BF BF_Base child, Basis function
 */
template
<class BF=Func_Base&>
class M_BLR: public M_Base {

    public:

        /** This constructor will preapare this object for either training
         *  or prediction (if potential is provides as a Config)
         *
         * Usage example:
         *
         * \code{.cpp}
         * Config config("Config");
         * M_BLR<BF_Linear> blr(config);
         * \endcode
         *
         */
        M_BLR(Config &c):
            config(c),
            bf(c),
            basis(c),
            desmat(bf,c),
            verbose(c.get<int>("VERBOSE"))
    {
        static_assert(std::is_base_of<BF_Base, BF>::value,
                "\nThe provided Basis function is not of a BasisFunction type.\n\
                For BLR use basis functions defined in basis_functions.h.\n\
                If you want to use this kernel functions use M_KRR class instead.\n\
                e.g. Use BF_Linear instead of Kern_Linear\n.");
        norm = Normaliser(c);

        if (config.exist("WEIGHTS"))
            read_pot();
    }

        /** This constructor will preapare this object for either training
         *  or prediction (if potential is provides as a Config)
         *
         * Usage example:

         * \code{.cpp}
         * Config config("Config");
         * BF_Linear bf(config);
         * M_BLR<> blr(bf, config);
         * \endcode
         *
         */
        M_BLR(BF &bf, Config &c):
            config(c),
            bf(bf),
            basis(c),
            desmat(bf,c),
            verbose(c.get<int>("VERBOSE"))
    {
        if (dynamic_cast<BF_Base*>(&bf) == nullptr)
            throw std::invalid_argument("Provided object is not of BF_Base type");

        static_assert(std::is_same<Func_Base&, BF>::value,
                "This constructor requires BF=Func_Base&\n");

        if (config.exist("WEIGHTS"))
            read_pot();

    }

        void train(StDescriptorsDB &st_desc_db, const StructureDB &stdb) {

            if(config.get<bool>("NORM"))
                norm = Normaliser(config,st_desc_db);

            desmat.build(st_desc_db,stdb);
            train(desmat);
        }

        void train(StructureDB &stdb, DC_Base &dc) {

            if(config.get<bool>("NORM")) {

                std::string force=config.get<std::string>("FORCE");
                std::string stress=config.get<std::string>("STRESS");

                config.remove("FORCE");
                config.remove("STRESS");
                config.add("FORCE", "false");
                config.add("STRESS", "false");

                StDescriptorsDB st_desc_db_temp = dc.calc(stdb);

                if(config.get<bool>("NORM")) {
                    norm = Normaliser(config);
                    norm.learn(st_desc_db_temp);
                    // norm.normalise(st_desc_db_temp);
                }

                config.remove("FORCE");
                config.remove("STRESS");
                config.add("FORCE", force);
                config.add("STRESS", stress);
            }

            desmat.build(stdb,norm,dc);
            train(desmat);
        }


        double epredict(const aed_rctype &aed) {
            return bf.epredict(nweights,aed);
        };

        double fpredict(const fd_type &fdij, const aed_rctype &aedi, const size_t k) {
            return bf.fpredict(nweights,fdij,aedi,k);
        }

        force_type fpredict(const fd_type &fdij, const aed_rctype &aedi) {
            return bf.fpredict(nweights,fdij,aedi).array();
        }

        //Structure predict(const Config &c, StDescriptors &std, const Structure &st) {
        //    if(config.get<bool>("NORM") && !std.normalised && bf.get_label()!="BF_Linear")
        //        norm.normalise(std);
        //    return M_Base::predict(c,std,st);
        //}

        StructureDB predict(Config &c, const StructureDB &stdb, DC_Base &dc) {
            return M_Base::predict(c,stdb,dc);
        }

        //StructureDB predict(const Config &c,
        //        /*not const*/ StDescriptorsDB &st_desc_db,
        //        const StructureDB &stdb) {
        //    return M_Base::predict(c,st_desc_db,stdb);
        //}

        // VARIOUS
        t_type get_weights() {
            return nweights;
        }
        t_type get_weights_uncertainty() {
            double lambda=config.get<double>("LAMBDA");
            if(lambda >= 0) throw std::runtime_error(
                    "Sigma matrix is only computed for LAMBDA < 0");
            return Sigma.diagonal();
        }
        void set_weights(t_type w) {
            nweights = w;
        };
        Config get_param_file() {
            Config c = config;
            //c.remove("ALPHA");
            //c.remove("BETA");
            c.remove("DBFILE");
            c.remove("FORCE");
            c.remove("STRESS");
            c.remove("VERBOSE");

            c.clear_internal_keys();
            c.remove("MODEL");
            c.add("MODEL", label);
            c.add("MODEL", bf.get_label());

            for (long int i=0;i<nweights.size();++i) {
                c.add("WEIGHTS", nweights(i,0));
            }

            if(config.get<bool>("NORM")) {
                for (long int i=0;i<norm.mean.size();++i) {
                    c.add("NMEAN", norm.mean(i,0));
                }
                for (long int i=0;i<norm.std_dev.size();++i) {
                    c.add("NSTDEV", norm.std_dev(i,0));
                }
            }
            return c;
        }
        StructureDB predict(Config config_pred, StructureDB &stdb, DC_Base &dc,
                t_type &predicted_error) {

            LinearRegressor::read_sigma(config_pred,Sigma);
            DesignMatrix<BF> dm(bf,config_pred);
            dm.scale=false; // do not scale energy, forces and stresses
            dm.build(stdb,norm,dc);

            phi_type &phi = dm.Phi;

            // compute error
            predicted_error = (phi*Sigma*phi.transpose()).diagonal();
            double beta = config.get<double>("BETA");
            predicted_error.array() += 1.0/beta;

            // compute energy, forces and stresses
            t_type Tpred = phi*weights;

            // Construct StructureDB object with predicted values
            StructureDB stdb_;
            stdb_.structures.resize(stdb.size());
            size_t i=0;
            for (size_t s=0; s<stdb.size(); ++s) {
                stdb_(s) = Structure(stdb(s));

                predicted_error(i) = sqrt(predicted_error(i));

                stdb_(s).energy = Tpred(i++);
                if (config_pred.get<bool>("FORCE")) {
                    for (size_t a=0; a<stdb(s).natoms(); ++a) {
                        for (size_t k=0; k<3; ++k) {
                            predicted_error(i) = sqrt(predicted_error(i));
                            stdb_(s).atoms[a].force[k] = Tpred(i++);
                        }
                    }
                }
                if (config_pred.get<bool>("STRESS")) {
                    for (size_t x=0; x<3; ++x) {
                        for (size_t y=x; y<3; ++y) {
                            predicted_error(i) = sqrt(predicted_error(i));
                            stdb_(s).stress(x,y) = Tpred(i++);
                            if (x!=y)
                                stdb_(s).stress(y,x) = stdb_(s).stress(x,y);
                        }
                    }
                }
            }
            return stdb_;
        }
        StructureDB predict(StructureDB &stdb) {
            if(!trained) throw std::runtime_error("This object is not trained!\n\
                    Hint: check different predict() methods.");

            phi_type &Phi = desmat.Phi;
            //t_type &Tlabels = desmat.Tlabels;

            // compute energy, forces and stresses
            t_type Tpred = Phi*weights;

            double eweightglob=config.get<double>("EWEIGHT");
            double fweightglob=config.get<double>("FWEIGHT");
            double sweightglob=config.get<double>("SWEIGHT");

            // Construct StructureDB object with predicted values
            StructureDB stdb_;
            stdb_.structures.resize(stdb.size());
            size_t s=0;
            long int i=0;
            while (i<Phi.rows()) {

                stdb_(s).energy = Tpred(i++)*stdb(s).natoms()/eweightglob/stdb(s).eweight;
                if (config.get<bool>("FORCE")) {
                    stdb_(s).atoms.resize(stdb(s).natoms());
                    for (size_t a=0; a<stdb(s).natoms(); ++a) {
                        for (size_t k=0; k<3; ++k) {
                            stdb_(s).atoms[a].force[k] = Tpred(i++)/fweightglob/stdb(s).fweight;
                        }
                    }
                }
                if (config.get<bool>("STRESS")) {
                    for (size_t x=0; x<3; ++x) {
                        for (size_t y=x; y<3; ++y) {
                            stdb_(s).stress(x,y) = Tpred(i++)/sweightglob/stdb(s).sweight;
                            if (x!=y)
                                stdb_(s).stress(y,x) = stdb_(s).stress(x,y);
                        }
                    }
                }
                s++;
            }
            return stdb_;
        }

    private:
        using mat=Eigen::MatrixXd;
        //Normaliser norm;
        std::string label="M_BLR";
        Config &config;
        BF bf;
        Basis<BF> basis;
        DesignMatrix<BF> desmat;
        bool trained=false;
        //double lambda;
        int verbose;
        t_type weights;     //used for training only.
        mat Sigma;

        // nweights are used for prediction.
        // These are the same as weights unless Linear BF is used.
        // For Linear BF weights are "normalised" such that
        // prediction can be made with non-normalised descriptor
        t_type nweights;

        void read_pot() {
            nweights.resize(config.size("WEIGHTS"));
            config.get<t_type>("WEIGHTS",nweights);

            if (config.get<bool>("NORM")) {
                norm.std_dev.resize(config.size("NSTDEV"));
                norm.mean.resize(config.size("NMEAN"));
                norm.bias = config.get<bool>("BIAS");
                config.get<Eigen::VectorXd>("NSTDEV",norm.std_dev);
                config.get<Eigen::VectorXd>("NMEAN",norm.mean);
            }
            // get weights from nweights after normaliser setup
            convert_to_weights();
            trained = true;
        }

        // TRAINING
        void train(phi_type &Phi, t_type &T) {
            if(trained) throw std::runtime_error("This object is already trained!");

            LinearRegressor::train(config,Phi, T,weights,Sigma);
            convert_to_nweights();

            //nweights *= desmat.e_std_dev;  // final weights in units of energy
            trained=true;
        }

        // normalise weights such that when predict is called
        // we can supply it with a non-normalised descriptor
        void convert_to_nweights() {
            if(config.get<bool>("NORM") && bf.get_label()=="BF_Linear") {
                nweights.resize(weights.rows());
                nweights.setZero();
                nweights(0) = weights(0);
                for (long int i=1; i<weights.size(); ++i) {

                    if (norm.std_dev(i) > std::numeric_limits<double>::min())
                        nweights(i) = weights(i) / norm.std_dev(i);
                    else
                        nweights(i) = weights(i);

                    nweights(0) -= norm.mean(i)*nweights(i);

                }
            }
            else {
                nweights=weights;
            }
        }
        // The opposite of convert_to_nweights()
        void convert_to_weights() {
            if(config.get<bool>("NORM") && bf.get_label()=="BF_Linear") {
                // convert normalised weights back to "normal"
                weights.resize(nweights.rows());
                weights.setZero();
                weights(0) = nweights(0);
                for (long int i=1; i<nweights.size(); ++i) {

                    if (norm.std_dev(i) > std::numeric_limits<double>::min())
                        weights(i) = nweights(i) * norm.std_dev(i);
                    else
                        weights(i) = nweights(i);

                    weights(0) += nweights(i)*norm.mean(i);

                }
            }
            else {
                weights = nweights;
            }
        }

        template <typename D>
            void train(D &desmat) {
                phi_type &Phi = desmat.Phi;
                t_type &T = desmat.T;
                train(Phi,T);
            }

};
#endif
