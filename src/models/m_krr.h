#ifndef M_KRR_H
#define M_KRR_H

#include "m_base.h"
#include "linear_regressor.h"
#include "../config.h"
#include "../descriptors_calc.h"

#include "../descriptors/d_all.h"
#include "../kernels/kern_all.h"
#include "design_matrix.h"
#include <stdexcept>
#include "basis.h"
#include "ekm.h"

/** Kernel Ridge Regression implemented via Empirical Kernel Map
 *
 * **Optional Config keys**:
 *
 *  - \ref LAMBDA = 0  -> use OLS
 *  - \ref LAMBDA > 0  -> manually set to given value
 *  - \ref LAMBDA = -1 -> use evidence approximation to find \f$\lambda\f$
 *  - \ref SBASIS N    -> Use N basis functions when nonlinear K is used
 *
 * @tparam K Kern_Base child, Kernel function
 */
template
<typename K=Func_Base&>
class M_KRR: public M_Base, public LinearRegressor {


    public:

        /** This constructor will preapare this object for either training
         *  or prediction (if potential is provides as a Config)
         *
         * Usage example:
         *
         * \code{.cpp}
         * Config config("Config");
         * M_KRR<Kern_Linear> blr(config);
         * \endcode
         *
         */
        M_KRR(Config &c):
            config(c),
            kernel(c),
            basis(c),
            ekm(c),
            desmat(kernel,c),
            verbose(c.get<int>("VERBOSE"))
    {
        static_assert(std::is_base_of<Kern_Base, K>::value,
                "\nThe provided Kernel K is not of a Kernel type.\n\
                For KRR use kernels defined in kernel.h.\n\
                e.g. Use Kern_Linear instead of BF_Linear\n.");
        norm = Normaliser(c);

        if (config.exist("WEIGHTS"))
            read_pot();
    }

        /** This constructor will preapare this object for either training
         *  or prediction (if potential is provides as a Config)
         *
         * Usage example:
         *
         * \code{.cpp}
         * Config config("Config");
         * Kern_Linear kern(config);
         * M_KRR<> krr(kern, config);
         * \endcode
         *
         */
        M_KRR(K &k, Config &c):
            config(c),
            kernel(k),
            basis(c),
            ekm(k),
            desmat(k,c),
            verbose(c.get<int>("VERBOSE"))
    {

        if (dynamic_cast<Kern_Base*>(&k) == nullptr)
            throw std::invalid_argument("Provided object is not of Kern_Base type");

        static_assert(std::is_same<Func_Base&, K>::value,
                "This constructor requires K=Func_Base&\n");

        if (config.exist("WEIGHTS"))
            read_pot();
    }


        void train(StructureDB &stdb, DC_Base &dc)
        {

            if(config.get<bool>("NORM") || kernel.get_label()!="Kern_Linear") {
                // To normalise we have to first learn std. dev. and means
                // for all AED. so we compute them first into temporary
                // StDescriptorsDB object.
                //
                // If nonlinear kernel is used we will need to select
                // basis vectors out of aeds pool.
                //
                // Use temp Config object
                // so we do not mess any keys such as DSIZE.
                // reset some keys so we calc AEDs only

                std::string force=config.get<std::string>("FORCE");
                std::string stress=config.get<std::string>("STRESS");

                config.remove("FORCE");
                config.remove("STRESS");
                config.add("FORCE", "false");
                config.add("STRESS", "false");

                StDescriptorsDB st_desc_db_temp = dc.calc(stdb);

                if(config.get<bool>("NORM")) {
                    norm = Normaliser(config);
                    norm.learn(st_desc_db_temp);
                    // norm.normalise(st_desc_db_temp);
                }

                config.remove("FORCE");
                config.remove("STRESS");
                config.add("FORCE", force);
                config.add("STRESS", stress);

                if (kernel.get_label()!="Kern_Linear") {
                    basis.build_random_basis(config.get<size_t>("SBASIS"),st_desc_db_temp);
                    desmat.mode.set_basis(basis.b);
                    kernel.set_basis(basis.b);
                    // to configure ekm, we need basis vectors
                    ekm.configure(basis.b);
                }
            }

            desmat.build(stdb,norm,dc);
            phi_type &Phi = desmat.Phi;
            t_type &T = desmat.T;
            train(Phi,T);
        }

        void train(StDescriptorsDB &st_desc_db, const StructureDB &stdb) {

            if(config.get<bool>("NORM"))
                norm = Normaliser(config,st_desc_db);

            if (kernel.get_label()!="Kern_Linear") {
                basis.build_random_basis(config.get<size_t>("SBASIS"),st_desc_db);
                desmat.mode.set_basis(basis.b);
                kernel.set_basis(basis.b);
                // to configure ekm, we need basis vectors
                ekm.configure(basis.b);
            }

            desmat.build(st_desc_db,stdb);
            phi_type &Phi = desmat.Phi;
            t_type &T = desmat.T;

            train(Phi,T);
        }


        double epredict(const aed_rctype &aed) {
            return kernel.epredict(kweights,aed);
        };

        double fpredict(const fd_type &fdij, const aed_rctype &aedi, const size_t k) {
            return kernel.fpredict(kweights,fdij,aedi,k);
        }

        force_type fpredict(const fd_type &fdij, const aed_rctype &aedi) {
            return kernel.fpredict(kweights,fdij,aedi);
        }

        //Structure predict(const Config &c, StDescriptors &std, const Structure &st) {
        //    // use config from the model
        //    if(config.get<bool>("NORM") && !std.normalised
        //            && kernel.get_label()!="Kern_Linear")
        //        norm.normalise(std);
        //    return M_Base::predict(c,std,st);
        //}

        StructureDB predict(Config &c, const StructureDB &stdb, DC_Base &dc) {
            return M_Base::predict(c,stdb,dc);
        }

        //StructureDB predict(const Config &c,
        //        /*not const*/ StDescriptorsDB &st_desc_db,
        //        const StructureDB &stdb) {
        //    return M_Base::predict(c,st_desc_db,stdb);
        //}

        // VARIOUS
        t_type get_weights() {
            return kweights;
        }
        t_type get_weights_uncertainty() {
            return Sigma.diagonal();
        }
        void set_weights(t_type w) {
            kweights = w;
        };
        Config get_param_file() {
            Config c = config;
            c.remove("ALPHA");
            c.remove("BETA");
            c.remove("DBFILE");
            c.remove("FORCE");
            c.remove("STRESS");
            c.remove("VERBOSE");

            c.clear_internal_keys();
            c.remove("MODEL");
            c.add("MODEL", label);
            c.add("MODEL", kernel.get_label());

            for (long int i=0;i<kweights.size();++i) {
                c.add("WEIGHTS", kweights(i,0));
            }

            if(config.get<bool>("NORM")) {
                for (long int i=0;i<norm.mean.size();++i) {
                    c.add("NMEAN", norm.mean(i,0));
                }
                for (long int i=0;i<norm.std_dev.size();++i) {
                    c.add("NSTDEV", norm.std_dev(i,0));
                }
            }
            if (kernel.get_label()!="Kern_Linear") {
                // dump basis to the config file file
                // make sure keys are not accidently assigned
                if (c.exist("SBASIS"))
                    c.remove("SBASIS");
                if (c.exist("BASIS"))
                    c.remove("BASIS");
                c.add("SBASIS", basis.b.cols());
                for (long int i=0;i<basis.b.cols();++i) {
                    for (long int j=0;j<basis.b.rows();++j) {
                        c.add("BASIS", basis.b(j,i));
                    }
                }
            }
            return c;
        }
        StructureDB predict(Config config_pred, StructureDB &stdb, DC_Base &dc,
                t_type &predicted_error) {

            LinearRegressor::read_sigma(config_pred,Sigma);
            DesignMatrix<K> dm(kernel,config_pred);
            dm.scale=false; // do not scale energy, forces and stresses
            dm.build(stdb,norm,dc);

            phi_type &phi = dm.Phi;

            // compute error
            predicted_error = (phi*Sigma*phi.transpose()).diagonal();
            double beta = config.get<double>("BETA");
            predicted_error.array() += 1.0/beta;

            // compute energy, forces and stresses
            t_type Tpred = phi*weights;

            // Construct StructureDB object with predicted values
            StructureDB stdb_;
            stdb_.structures.resize(stdb.size());
            size_t i=0;
            for (size_t s=0; s<stdb.size(); ++s) {
                stdb_(s) = Structure(stdb(s));

                predicted_error(i) = sqrt(predicted_error(i));

                stdb_(s).energy = Tpred(i++);
                if (config_pred.get<bool>("FORCE")) {
                    for (size_t a=0; a<stdb(s).natoms(); ++a) {
                        for (size_t k=0; k<3; ++k) {
                            predicted_error(i) = sqrt(predicted_error(i));
                            stdb_(s).atoms[a].force[k] = Tpred(i++);
                        }
                    }
                }
                if (config_pred.get<bool>("STRESS")) {
                    for (size_t x=0; x<3; ++x) {
                        for (size_t y=x; y<3; ++y) {
                            predicted_error(i) = sqrt(predicted_error(i));
                            stdb_(s).stress(x,y) = Tpred(i++);
                            if (x!=y)
                                stdb_(s).stress(y,x) = stdb_(s).stress(x,y);
                        }
                    }
                }
            }
            return stdb_;
        }
        StructureDB predict(StructureDB &stdb) {
            if(!trained) throw std::runtime_error("This object is not trained!\n\
                    Hint: check different predict() methods.");

            phi_type &Phi = desmat.Phi;
            //t_type &Tlabels = desmat.Tlabels;

            // compute energy, forces and stresses
            t_type Tpred = Phi*weights;

            double eweightglob=config.get<double>("EWEIGHT");
            double fweightglob=config.get<double>("FWEIGHT");
            double sweightglob=config.get<double>("SWEIGHT");

            // Construct StructureDB object with predicted values
            StructureDB stdb_;
            stdb_.structures.resize(stdb.size());
            size_t s=0;
            long int i=0;
            while (i<Phi.rows()) {

                stdb_(s).energy = Tpred(i++)*stdb(s).natoms()/eweightglob/stdb(s).eweight;
                if (config.get<bool>("FORCE")) {
                    stdb_(s).atoms.resize(stdb(s).natoms());
                    for (size_t a=0; a<stdb(s).natoms(); ++a) {
                        for (size_t k=0; k<3; ++k) {
                            stdb_(s).atoms[a].force[k] = Tpred(i++)/fweightglob/stdb(s).fweight;
                        }
                    }
                }
                if (config.get<bool>("STRESS")) {
                    for (size_t x=0; x<3; ++x) {
                        for (size_t y=x; y<3; ++y) {
                            stdb_(s).stress(x,y) = Tpred(i++)/sweightglob/stdb(s).sweight;
                            if (x!=y)
                                stdb_(s).stress(y,x) = stdb_(s).stress(x,y);
                        }
                    }
                }
                s++;
            }
            return stdb_;
        }

    private:
        using mat=Eigen::MatrixXd;
        Config &config;
        K kernel;
        Basis<K> basis;
        EKM<K> ekm;
        DesignMatrix<K> desmat;
        std::string label="M_KRR";
        bool trained=false;
        t_type weights;     // used for trainig only
        t_type kweights;    // these are used for all predictions
        mat Sigma;
        int verbose;

        void read_pot() {
            kweights.resize(config.size("WEIGHTS"));
            config.get<t_type>("WEIGHTS",kweights);

            if (config.get<bool>("NORM")) {
                norm.std_dev.resize(config.size("NSTDEV"));
                norm.mean.resize(config.size("NMEAN"));
                config.get<Eigen::VectorXd>("NSTDEV",norm.std_dev);
                config.get<Eigen::VectorXd>("NMEAN",norm.mean);
            }
            if (kernel.get_label()!="Kern_Linear") {
                basis.read_basis_from_config();
                kernel.set_basis(basis.b);
            }
            convert_to_weights();
            trained = true;
        }

        void train(phi_type &Phi, t_type &T) {
            if(trained) throw std::runtime_error("This object is already trained!");

            if (kernel.get_label()!="Kern_Linear") {
                // project PHI using ekm
                ekm.project(Phi);
            }

            LinearRegressor::train(config,Phi, T,weights,Sigma);
            convert_to_nweights();

            //kweights *= e_std_dev;  // final weights in units of energy
            trained=true;
        }

        void convert_to_nweights() {
            if (kernel.get_label()!="Kern_Linear") {
                //kernalize weights
                kweights = ekm.KK.transpose()*weights;
            }
            else if(config.get<bool>("NORM") && kernel.get_label()=="Kern_Linear") {
                // normalise weights such that when predict is called
                // we can supply it with a non-normalised descriptor
                kweights.resize(weights.rows());
                kweights(0) = weights(0);
                for (long int i=1; i<weights.size(); ++i) {

                    if (norm.std_dev(i) > std::numeric_limits<double>::min())
                        kweights(i) = weights(i) / norm.std_dev(i);
                    else
                        kweights(i) = weights(i);

                    kweights(0) -= norm.mean(i)*kweights(i);

                }
            }
            else {
                kweights=weights;
            }
        }
        // The opposite of convert_to_nweights()
        void convert_to_weights() {
            if(config.get<bool>("NORM") && kernel.get_label()=="Kern_Linear") {
                // convert normalised weights back to "normal"
                weights.resize(kweights.rows());
                weights.setZero();
                weights(0) = kweights(0);
                for (long int i=1; i<kweights.size(); ++i) {

                    if (norm.std_dev(i) > std::numeric_limits<double>::min())
                        weights(i) = kweights(i) * norm.std_dev(i);
                    else
                        weights(i) = kweights(i);

                    weights(0) += kweights(i)*norm.mean(i);

                }
            }
            else {
                weights = kweights;
            }
        }
};
#endif
