#ifndef MODEL_H
#define MODEL_H

#include "../libs/Eigen/Dense"
#include "../structure.h"
#include "../structure_db.h"
#include "../st_descriptors.h"
#include "../st_descriptors_db.h"
#include "../normaliser.h"
#include "../utils/registry.h"
//#include "../descriptors_calc.h"
#include "../dc_base.h"

using fd_type=StDescriptors::fd_type;
using aed_type=StDescriptors::aed_type;
using aed_rtype=StDescriptors::aed_rtype;
using aed_rctype=StDescriptors::aed_rctype;
using sd_type=StDescriptors::sd_type;
using force_type=Eigen::Vector3d;
using stress_type=Structure::stress_type;

/** Base class for all models.
 *
 *  All models **must** inherit this class and implement all methods.
 *
 *  Naming convention used: M_ModelName
 *
 */
class M_Base {

    public:
        Normaliser norm;
        virtual ~M_Base() {};

        /** \brief Predict local energy of an atom or bond energy.
         *
         *  The result depends on how aed is computed.
         *
         *  If it is computed between a pair of atoms than the result is a bond energy.
         *
         *  If aed contains sum over all nearest neighbours than the result is
         *  a local atomic energy \f$ E_i \f$.
         * */
        virtual double epredict(const aed_rctype &aed)=0;

        /** \brief Predict total energy for a set of atoms. */
        double epredict(const StDescriptors &std);

        /** \brief Predict force between a pair of atoms in a k-direction. */
        virtual double fpredict(const fd_type &fdij, const aed_rctype &aedi, size_t k)=0;

        /** \brief Predict force between a pair of atoms. */
        virtual force_type fpredict(const fd_type &fdij,
                const aed_rctype &aedi)=0;

        /** \brief Predict total force on an atom a. */
        virtual void fpredict(const size_t a, force_type &v,
                const StDescriptors &std, const Structure &st);

        /** \brief Predict total force on an atom a. */
        virtual force_type fpredict(const size_t a, const StDescriptors &std,
                const Structure &st);

        /** \brief Predict energy, forces and stresses for the Structure st. */
        virtual Structure predict(const Config &c,
                /*not const*/ StDescriptors &std, const Structure &st);

        /** \brief Predict energy, forces and stresses for a set of Structures.
         *
         * Use precalculated descriptors.
         */
        virtual StructureDB predict(const Config &c,
                /*not const*/ StDescriptorsDB &st_desc_db,
                const StructureDB &stdb);

        /** \brief Predict energy, forces and stresses for a set of Structures. */
        virtual StructureDB predict(Config &c, const StructureDB &stdb, DC_Base &dc);

        /** \brief Predict virial stresses in a Structure st. */
        virtual stress_type spredict(const StDescriptors &std, const Structure &st);

        /** \brief Predict virial stresses in all Structures. */
        virtual stress_type spredict(const size_t a, const StDescriptors &std,
                const Structure &st);

        /** \brief Predict virial stress for atom a in a Structure st. */
        virtual void spredict(const size_t a, stress_type &s,
                const StDescriptors &std, const Structure &st);

        // VARIOUS
        virtual t_type get_weights()=0;
        virtual t_type get_weights_uncertainty()=0;
        virtual void set_weights(t_type w)=0;
        virtual Config get_param_file()=0;

        // TRAINING

        /** This will fit a model without precalculated StDescriptorsDB object.
         *
         * Structure stdb object must have all nearest neighbours calculated
         * with NN_Finder.
         *
         * @param dc is a DescriptorCalc object
         */
        virtual void train(StructureDB &, DC_Base &) {};


        /** This will fit a model with precalculated StDescriptorsDB object.
         *
         * Structure stdb object must have all nearest neighbours calculated
         * with NN_Finder.
         */
        virtual void train(StDescriptorsDB &, const StructureDB &) {};

        virtual StructureDB predict(Config config_pred, StructureDB &stdb, DC_Base &dc,
                t_type &predicted_error)=0;

        virtual StructureDB predict(StructureDB &stdb)=0;

};
#endif
