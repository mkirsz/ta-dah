#ifdef TADAH_ENABLE_MPI
#include <mpi.h>
#endif

#ifndef TADAH_CLI_H
#define TADAH_CLI_H
#include <ios>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <stdexcept>
#include <string>
#include <vector>
#include "libs/CLI11/CLI11.hpp"
#include "libs/CLI11/Timer.hpp"
#include "config.h"
#include "dc_selector.h"
#include "descriptors_calc.h"
#include "func_base.h"
#include "models/m_base.h"
#include "structure_db.h"
#include "nn_finder.h"
#include "utils/utils.h"
#include "analytics/analytics.h"
#include "hyper_param/hpo.h"
#include "version.h"


namespace fs = std::filesystem;
class TadahCLI {
    private:
        CLI::App &app;
        bool verbose=false;
    public:
        // PREDICTION
        std::string pot_file;
        std::string config_file;
        std::string config_tpred;
        std::string target_file;
        std::string targets_dir;
        std::string targets_out_dir;
        std::vector<std::string> datasets;
        TadahCLI(CLI::App &_app):
            app(_app)
    { }
        void subcommand_train();
        void subcommand_predict();
        void subcommand_hpo(int argc, char**argv);
        void set_verbose();
        bool is_verbose();
        void flag_version();
        std::vector<fs::path> read_targets(std::string path);

        /* Return a vector of absolute paths to every file in a directory.
         * Subdirectories are ommited.
         */
        std::vector<fs::path> read_files(std::string path);

};

#endif

