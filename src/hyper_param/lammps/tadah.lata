# Find minimum energy bcc/hcp/fcc configuration
# Mark Tschopp, 2010
# Modified 2022 by M.Kirsz

#variable lata equal 3.3

# Set wall time limit on this script
timer timeout ${tout}
variable GPa_press equal ${P}*10000

# ---------- Initialize Simulation --------------------- 
clear 
units metal 
dimension 3 
boundary p p p 
atom_style atomic 
atom_modify map array

# ---------- Create Atoms --------------------- 
lattice 	${structure} ${lata_guess}
region	box block 0 1 0 1 0 1 units lattice
create_box	1 box

lattice	${structure} ${lata_guess} orient x 1 0 0 orient y 0 1 0 orient z 0 0 1  
create_atoms 1 box
replicate 1 1 1

mass 1 1e-20    # to keep lammps happy for some potentials

# ---------- Define Interatomic Potential --------------------- 

pair_style tadah/tadah
pair_coeff * * pot.tadah ${element}
neighbor 2.0 bin 
neigh_modify every 1 delay 0 check yes 
 
# ---------- Define Settings --------------------- 
compute eng all pe/atom 
compute eatoms all reduce sum c_eng 

# ---------- Run Minimization --------------------- 
reset_timestep 0 
fix 1 all box/relax iso ${GPa_press} vmax 0.001
thermo 0 
thermo_style custom step pe lx ly lz press pxx pyy pzz c_eatoms 
min_style cg 
minimize 1e-15 1e-15 5000 10000 

variable natoms equal "count(all)" 
variable teng equal "c_eatoms"
variable length equal "lx"
variable ecoh equal "v_teng/v_natoms"

print "========================================="
print "Total energy (eV) = ${teng};"
print "Number of atoms = ${natoms};"
print "Lattice constant (Angstoms) = ${length};"
print "Cohesive energy (eV) = ${ecoh};"
print "========================================="
