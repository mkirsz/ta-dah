#ifndef HYPER_PARAM_OPTI_H
#define HYPER_PARAM_OPTI_H

#include <chrono>
#include <vector>
#include <cmath>
#include "../config.h"
#include "../structure_db.h"
#include "../nn_finder.h"
#include "../dc_selector.h"
#include "../models/m_base.h"
#include "../func_base.h"
#include "../descriptors_calc.h"
#include "../analytics/analytics.h"
#include "../utils/utils.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "hpo_targets.h"
#include "glf.h"
#include "lammps_scripts.h"

#if defined(TADAH_ENABLE_OPENMP)
#include <omp.h>
#else
typedef int omp_int_t;
inline omp_int_t omp_get_thread_num() { return 0;}
inline omp_int_t omp_get_max_threads() { return 1;}
inline omp_int_t omp_get_num_threads() { return 1;}
#endif

/* General comments:
 * - lammps must be compiled as a shared library
 * - lammps must be compiled with c++ exceptions: -DLAMMPS_EXCEPTIONS
 * - when compiling code which uses below classes link -llammps -ldlib -fopenmp
 * - The code might compile without -fopenmp but on some systems this causes
 *   numerical instabilties. This is (probably) only true for HyperParamOpti class.
 */

class HyperParamOptiBase {
};

/**
 * Usage example:
 *
 *     Config config("config.train");
 *     HPOTargets trg("targets");
 *     HyperParamOpti<dlib::matrix<double,0,1>> hpo(config,trg);
 *     # the object is now ready to passed to the optimiser.
 *
 * Supported Config keys:
 * RCUT2B,RCUTMB,CGRID2B,CGRIDMB,SGRID2B,SGRIDMB,CEMBFUNC,SEMBFUNC
 *
 * This class is very fragile.
 * There is no error checking of the user input whatsoever.
 *
 */
template <typename T, typename GLF, typename R>
class HyperParamOpti: public HyperParamOptiBase {
    private:
        std::string element = "Ta";     // TODO
        bool fail=false;
        R runner;
        LammpsScripts scr;
        HPOTargets &trg;
        GLF glf;
        std::string delim=" ";
        double best_score=std::numeric_limits<double>::max();
        Config config;
        StructureDB stdb;
        size_t it=0;    // iteration number
        size_t pevery=std::numeric_limits<size_t>::max();   // Dump potential every this steps
        std::string pevery_dir;

        // Expand parameter pack with delim and return as string.
        template <typename... Args>
            std::string as_string(Args... args) {
                std::stringstream ss;
                using expander = int[];
                (void)expander{0, (void(ss << std::setprecision(15) << std::forward<Args>(args) << delim), 0)...};
                return ss.str().substr(0,ss.str().size()-delim.size()); // remove last delim
            }

        // helper method to update config cutoff
        void rc (std::string s, double rc) {
            config.remove(s);
            config.add(s,rc);
        };

        // helper method to update config grid
        template <typename... Args>
            void grid(std::string gridname, Args ...args){
                config.remove(gridname);
                config.add(gridname,as_string(args...));
            }


        void update_config (T &p) {


            for (auto &c : trg.configmap) {
                config.remove(c);
            }
            for (size_t i=0; i<trg.configmap.size();++i) {
                std::string s = trg.configmap[i];
                if (s=="MPARAMS") {
                    config.remove(s);
                    config.add(s,p(i));
                }
                if (s=="SBASIS") {
                    config.remove(s);
                    config.add(s,p(i));
                }
                if (s=="LAMBDA") {
                    config.remove(s);
                    config.add(s,p(i));
                }
                if (s=="RCUT2B" || s=="RCUTMB") rc(s,p(i));
                if (s=="CGRID2B" || s=="CGRIDMB") config.add(s,p(i));
                if (s=="SGRID2B" || s=="SGRIDMB") config.add(s,p(i));
                if (s=="CEMBFUNC" || s=="SEMBFUNC") config.add(s,p(i));

                if (s=="SWEIGHT") {
                    config.remove(s);
                    config.add(s,p(i));
                }
                if (s=="FWEIGHT") {
                    config.remove(s);
                    config.add(s,p(i));
                }

            }
        }
        int calc_lata(TrgData &pdata) {
#pragma omp parallel for shared(runner,fail,pdata,trg)
            for(size_t i = 0; i < trg.data.lata_size(); i++) {
                auto v = trg.data.m1.begin();
                advance(v, i);
                const std::string P = v->first;
                // Only lata calculations should set this to false.
                // When a calculation in the chain fails
                // the flag is set to fail and should stay
                // until the next iteration of the GOA.
                pdata.set_fail(P,false);
                int thread = omp_get_thread_num();
                auto lmp = scr.run_lata(runner,P, trg.data.get_structure(P),
                        trg.data.get_lata(P), element,
                        "log.lata"+std::to_string(thread),
                        trg.MAX_TIME);
                if (!lmp) {
                    fail = true;
                    pdata.set_fail(P,true);
                }
                else {
                    double a= stod(runner.get_variable(lmp,"length"));
                    double ecoh=stod(runner.get_variable(lmp,"ecoh"));

                    pdata.set_lata(P,a);
                    pdata.set_ecoh(P,ecoh);
                }
                if (lmp) delete lmp;
            }

            return 0;
        }
        int calc_uvacf(TrgData &pdata) {
#pragma omp parallel for shared(runner,fail,pdata,trg)
            for(size_t i = 0; i < trg.data.uvacf_size(); i++) {
                auto v = trg.data.m2.begin();
                advance(v, i);
                const std::string P = v->first;
                int thread = omp_get_thread_num();
                auto lmp = scr.run_uvacf(runner,P,trg.data.get_lata(P),
                        "log.uvacf"+std::to_string(thread),
                        trg.MAX_TIME);
                if (!lmp) {
                    fail = true;
                    pdata.set_fail(P,true);
                }
                else {
                    //double press= stod(runner.get_variable(lmp,"PRESS"));
                    //double a= stod(runner.get_variable(lmp,"latafinal"));
                    double E_v=stod(runner.get_variable(lmp,"Ev"));
                    double H_v=stod(runner.get_variable(lmp,"Hv"));

                    pdata.set_uEvf(P,E_v);
                    pdata.set_uHvf(P,H_v);
                }
                if (lmp) delete lmp;
            }
            return 0;
        }
        int calc_usurface(TrgData &pdata) {
#pragma omp parallel for shared(runner,fail,pdata,trg)
            for(size_t i = 0; i < trg.data.usurf_size(); i++) {
                auto v = trg.data.m4.begin();
                advance(v, i);
                const int hkl = v->first;
                int thread = omp_get_thread_num();
                auto lmp = scr.run_usurface(runner,hkl,
                        trg.data.get_lata("0"),
                        trg.data.get_structure("0"), element,
                        "log.lata"+std::to_string(thread),
                        trg.MAX_TIME);
                if (!lmp) {
                    fail = true;
                    pdata.set_fail("0",true);
                }
                else {
                    double E_hkl=stod(runner.get_variable(lmp,"E_f2"));

                    pdata.set_usurf(hkl,E_hkl);
                }
                if (lmp) delete lmp;
            }

            return 0;
        }
        int calc_surface(TrgData &pdata) {
#pragma omp parallel for shared(runner,fail,pdata,trg)
            for(size_t i = 0; i < trg.data.surf_size(); i++) {
                auto v = trg.data.m5.begin();
                advance(v, i);
                const int hkl = v->first;
                int thread = omp_get_thread_num();
                auto lmp = scr.run_surface(runner,hkl,
                        trg.data.get_lata("0"),
                        trg.data.get_structure("0"), element,
                        "log.lata"+std::to_string(thread),
                        trg.MAX_TIME);
                if (!lmp) {
                    fail = true;
                    pdata.set_fail("0",true);
                }
                else {
                    double E_hkl=stod(runner.get_variable(lmp,"E_f2"));

                    pdata.set_surf(hkl,E_hkl);
                }
                if (lmp) delete lmp;
            }

            return 0;
        }
        int calc_envol(TrgData &pdata) {
#pragma omp parallel for shared(runner,fail,pdata,trg)
            for(size_t i = 0; i < trg.data.envol_size(); i++) {
                auto v = trg.data.m7.begin();
                advance(v, i);
                const std::string lata = v->first;
                int thread = omp_get_thread_num();
                auto lmp = scr.run_envol(runner,lata,
                        trg.data.get_structure(),
                        element, "log.lata"+std::to_string(thread),
                        trg.MAX_TIME);
                if (!lmp) {
                    fail = true;
                    //pdata.set_fail(P,true); // this cannot fail
                }
                else {
                    double ecoh=stod(runner.get_variable(lmp,"ecoh"));

                    pdata.set_envol(lata,ecoh);
                }
                if (lmp) delete lmp;
            }

            return 0;
        }
    public:
        HyperParamOpti(Config c, HPOTargets &trg):
            trg(trg),
            glf(trg),
            config(c),
            stdb(c)
    {
        NNFinder nn(config);
        nn.calc(stdb);

        if (config.exist("HPOEVERY")) {
            pevery_dir=config.get<std::string>("HPOEVERY",0);
            pevery=config.get<size_t>("HPOEVERY",1);
        }

        // update CONFIG - constants specific to this target
        for (const auto p : trg.cfg) {
            std::string key=p.first;
            std::string val=p.second;
            config.remove(key);
            config.add(key,val);
        }
    }

        //double operator() (T args) {
        double operator() (T p) {
            it++;
            using std::chrono::high_resolution_clock;
            using std::chrono::duration_cast;
            using std::chrono::duration;
            using std::chrono::milliseconds;

            fail=false;

            auto t1 = high_resolution_clock::now();
            //p = std::vector<double>(args.begin(), args.end());
            update_config(p);

            Config config_temp = config;
            config_temp.postprocess();

            DC_Selector DCS(config_temp);

            DescriptorsCalc<> dc(config_temp,*DCS.d2b,*DCS.d3b,*DCS.dmb,
                    *DCS.c2b,*DCS.c3b,*DCS.cmb);

            Func_Base *fb = factory<Func_Base,Config>(
                    config.get<std::string>("MODEL",1),config);

            M_Base *model = factory<M_Base,Func_Base,Config>
                (config_temp.get<std::string>("MODEL",0),*fb,config_temp);

            auto t2 = high_resolution_clock::now();
            duration<double, std::milli> t21 = t2 - t1;
            //std::cout << "T1: " << t21.count() << "ms\n";

            model->train(stdb,dc);
            auto t3 = high_resolution_clock::now();
            duration<double, std::milli> t32 = t3 - t2;
            //std::cout << "T2: " << t32.count() << "ms\n";

            // This potential will be used by LAMMPS
            Config pot_tadah = model->get_param_file();
            std::ofstream outfile;
            outfile.open ("pot.tadah");
            outfile << pot_tadah;
            outfile.close();

            auto t4 = high_resolution_clock::now();
            duration<double, std::milli> t43 = t4 - t3;
            //std::cout << "T3: " << t43.count() << "ms\n";

            // Do not remove me for a while...
            //pot_tadah.add("VERBOSE",1);
            //pot_tadah.add("FORCE","false");
            //pot_tadah.add("STRESS","false");
            //M_Base *modelp = factory<M_Base,Func_Base,Config>
            //    (pot_tadah.get<std::string>("MODEL",0),*fb,pot_tadah);
            //StructureDB stpred = modelp->predict(pot_tadah,stdb,dc);

            StructureDB stpred = model->predict(stdb);

            Analytics a(stdb,stpred);

            TrgData pdata = trg.data;
            if (trg.use_lata | trg.use_ecoh | trg.use_surface) {
                calc_lata(pdata);
            }

            // Vacancy formation energy/enthalpy at selected pressures
            if (trg.use_uEvf || trg.use_uHvf) {
                calc_uvacf(pdata);
            }
            if (trg.use_usurface) {
                calc_usurface(pdata);
            }
            if (trg.use_surface) {
                calc_surface(pdata);
            }
            if (trg.use_envol) {
                calc_envol(pdata);
            }

            double tloss = glf.total_loss(a,p,pdata,fail);

            // Dump potential if best
            if (tloss<best_score) {
                best_score = tloss;
                std::ofstream outfile;
                outfile.open ("best_pot.tadah");
                outfile << pot_tadah;
                outfile.close();
            }

            // Dump potential pevery steps
            // Controled by HPOEVERY
            if (it%pevery==0) {
                std::ofstream outfile;
                outfile.open (pevery_dir+"/pot_"+std::to_string(it)+".tadah");
                outfile << pot_tadah;
                outfile.close();
            }

            auto t5 = high_resolution_clock::now();
            duration<double, std::milli> t54 = t5 - t4;
            //std::cout << "T4: " << t54.count() << "ms\n";
            if(model)
                delete model;
            if(fb)
                delete fb;
            return tloss;
        }
};
#endif
