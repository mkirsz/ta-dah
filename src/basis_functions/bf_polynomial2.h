#ifndef BF_POLYNOMIAL2_H
#define BF_POLYNOMIAL2_H
#include "bf_base.h"

/** Polynomial order 2 basis function with c=0 */
struct BF_Polynomial2: BF_Base
{
    BF_Polynomial2();
    BF_Polynomial2(const Config &c);
    std::string label = "BF_Polynomial2";
    std::string get_label();
    double epredict(const t_type &weights, const aed_rctype& aed);
    double fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi, const size_t k);
    force_type fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi);
    size_t get_phi_cols(const Config &config);
    void  calc_phi_energy_row(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d);
    void  calc_phi_force_rows(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d);
    void  calc_phi_stress_rows(phi_type &Phi, size_t &row,
            const double fac[6], const Structure &st, const StDescriptors &st_d);
};
#endif
