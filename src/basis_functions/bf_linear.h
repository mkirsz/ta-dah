#ifndef BF_LINEAR_H
#define BF_LINEAR_H
#include "bf_base.h"
/** Linear basis function */
struct BF_Linear: public BF_Base
{
    BF_Linear();
    BF_Linear(const Config &c);
    std::string get_label();
    std::string label = "BF_Linear";
    double epredict(const t_type &weights, const aed_rctype& aed);
    double fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi, const size_t k);
    force_type fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi);
    size_t get_phi_cols(const Config &config);
    void calc_phi_energy_row(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d);
    void calc_phi_force_rows(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d);
    void calc_phi_stress_rows(phi_type &Phi, size_t &row,
            const double fac[6], const Structure &st, const StDescriptors &st_d);
};
#endif
