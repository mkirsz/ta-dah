#include "bf_polynomial2.h"

Registry<Func_Base>::Register<BF_Polynomial2> BF_Polynomial2_1( "BF_Polynomial2" );
Registry<Func_Base,Config>::Register<BF_Polynomial2> BF_Polynomial2( "BF_Polynomial2" );

BF_Polynomial2::BF_Polynomial2() {}
BF_Polynomial2::BF_Polynomial2(const Config &c)
{
    verbose = c.get<int>("VERBOSE");
    if (verbose) std::cout << std::endl << label << std::endl;
}
std::string BF_Polynomial2::get_label()
{
    return label;
}
double BF_Polynomial2::epredict(const t_type &weights, const aed_rctype& aed)
{
    size_t b=0;
    double res=0.0;
    for (long int i=0; i<aed.size(); ++i) {
        for (long int ii=i; ii<aed.size(); ++ii) {
            res+= weights(b++)*aed(i)*aed(ii);
        }
    }
    return res;
}
double BF_Polynomial2::fpredict(const t_type &weights, const fd_type &fdij,
        const aed_rctype& aedi, const size_t k)
{
    double res=0.0;
    size_t b=0;
    for (long int i=0; i<fdij.rows(); ++i) {
        for (long int ii=i; ii<fdij.rows(); ++ii) {
            res -= weights(b)*(fdij(i,k)*aedi(ii) + fdij(ii,k)*aedi(i));
            b++;
        }
    }
    return res;
}
force_type BF_Polynomial2::fpredict(const t_type &weights, const fd_type &fdij,
        const aed_rctype& aedi)
{
    force_type v(3);
    v.setZero();
    size_t b=0;
    for (long int i=0; i<fdij.rows(); ++i) {
        for (long int ii=i; ii<fdij.rows(); ++ii) {
            for (size_t k=0; k<3; ++k) {
                v(k) -= weights(b)*(fdij(i,k)*aedi(ii) + fdij(ii,k)*aedi(i));
            }
            b++;
        }
    }
    return v;
}
size_t BF_Polynomial2::get_phi_cols(const Config &config)
{
    size_t cols = config.get<size_t>("DSIZE");
    return (cols*cols+cols)/2;
}
void BF_Polynomial2::calc_phi_energy_row(phi_type &Phi, size_t &row,
        const double fac, const Structure &, const StDescriptors &st_d)
{
    for (size_t a=0; a<st_d.naed();++a) {
        const StDescriptors::aed_rctype& aed = st_d.get_aed(a);
        size_t b=0;
        for (size_t i=0; i<st_d.dim(); ++i) {
            for (size_t ii=i; ii<st_d.dim(); ++ii) {
                Phi(row,b++) += aed(i)*aed(ii);
            }
        }
    }
    Phi.row(row++) *= fac;
}
void BF_Polynomial2::calc_phi_force_rows(phi_type &Phi, size_t &row,
        const double fac, const Structure &st, const StDescriptors &st_d)
{
    for (size_t a=0; a<st.natoms(); ++a) {
        const StDescriptors::aed_rctype& aedi = st_d.get_aed(a);
        for (size_t jj=0; jj<st_d.fd[a].size(); ++jj) {
            const size_t j=st.near_neigh_idx[a][jj];
            size_t aa = st.get_nn_iindex(a,j,jj);
            const StDescriptors::fd_type &fdji = st_d.fd[j][aa];
            const StDescriptors::fd_type &fdij = st_d.fd[a][jj];
            const StDescriptors::aed_rctype& aedj = st_d.get_aed(j);
            size_t b=0;
            for (long int i=0; i<fdij.rows(); ++i) {
                for (long int ii=i; ii<fdij.rows(); ++ii) {
                    for (size_t k=0; k<3; ++k)
                        Phi(row+k,b) -= fac*(fdij(i,k)*aedi(ii)
                                + fdij(ii,k)*aedi(i)
                                - fdji(i,k)*aedj(ii)
                                -fdji(ii,k)*aedj(i));
                    b++;
                }
            }
        }
        row+=3;
    }
}
void BF_Polynomial2::calc_phi_stress_rows(phi_type &Phi, size_t &row,
        const double fac[6], const Structure &st, const StDescriptors &st_d)
{
    for (size_t a=0; a<st.natoms(); ++a) {
        const Eigen::Vector3d &ri = st(a).position;
        const StDescriptors::aed_rctype& aedi = st_d.get_aed(a);
        for (size_t jj=0; jj<st_d.fd[a].size(); ++jj) {
            const size_t j=st.near_neigh_idx[a][jj];
            size_t aa = st.get_nn_iindex(a,j,jj);
            const StDescriptors::fd_type &fdji = st_d.fd[j][aa];
            const StDescriptors::fd_type &fdij = st_d.fd[a][jj];
            const Eigen::Vector3d &rj = st.nn_pos(a,jj);
            const StDescriptors::aed_rctype& aedj = st_d.get_aed(j);
            size_t b=0;
            for (long int i=0; i<fdij.rows(); ++i) {
                for (long int ii=i; ii<fdij.rows(); ++ii) {
                    size_t mn=0;
                    for (size_t x=0; x<3; ++x) {
                        for (size_t y=x; y<3; ++y) {
                            Phi(row+mn,b) += 0.5*fac[mn]*(ri(x)-rj(x))
                                *(fdij(i,y)*aedi(ii) + fdij(ii,y)*aedi(i)
                                - fdji(i,y)*aedj(ii) - fdji(ii,y)*aedj(i));
                            mn++;
                        }
                    }
                    b++;
                }
            }
        }
    }
    row += 6;
}
