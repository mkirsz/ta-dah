#ifndef BASIS_FUNCTIONS_H
#define BASIS_FUNCTIONS_H
#include <iomanip>
#include <iostream>
#include <limits>
#include "../models/m_base.h"
#include "../utils/registry.h"
#include "../func_base.h"

/** \brief Base class to be used by all basis functions */
struct BF_Base: public Func_Base {
    /** \brief Return label of this object */
    int verbose;
    virtual std::string get_label()=0;
    virtual double epredict(const t_type &weights, const aed_rctype& aed)=0;
    virtual double fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi, const size_t k)=0;
    virtual force_type fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi)=0;
    virtual size_t get_phi_cols(const Config &config)=0;
    virtual void calc_phi_energy_row(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d)=0;
    virtual void calc_phi_force_rows(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d)=0;
    virtual void calc_phi_stress_rows(phi_type &Phi, size_t &row,
            const double fac[6], const Structure &st, const StDescriptors &st_d)=0;
};
#endif
