#include "d3_dummy.h"

Registry<D3_Base,Config>::Register<D3_Dummy> D3_Dummy_1( "D3_Dummy" );

D3_Dummy::D3_Dummy() {}
D3_Dummy::D3_Dummy(Config &) {}

void D3_Dummy::calc_aed(
        const size_t,
        const double,
        const double,
        const double,
        const double,
        StDescriptors::aed_rtype ) {}
void D3_Dummy::calc_fd(
        const size_t,
        const double,
        const double,
        const double,
        const double,
        const double,
        const double,
        StDescriptors::fd_type &) {}
void D3_Dummy::calc_all(
        const size_t,
        const double,
        const double,
        const double,
        const double,
        const double,
        const double,
        StDescriptors::aed_rtype ,
        StDescriptors::fd_type &) {}
size_t D3_Dummy::size() { return s;}
std::string D3_Dummy::label() {return lab;}
