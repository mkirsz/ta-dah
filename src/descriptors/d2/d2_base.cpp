#include "d2_base.h"
template<> Registry<D2_Base,Config>::Map Registry<D2_Base,Config>::registry{};
void D2_Base::calc_fd_approx(
        const double ,
        const double ,
        const double ,
        StDescriptors::aed_rtype aedp,
        StDescriptors::aed_rtype aedn,
        StDescriptors::fd_type &fd_ij,
        const double h) {

    for (size_t i=0; i<(*this).size(); ++i)
        fd_ij(i,0) = (aedp(i)-aedn(i))/(2*h);
}
