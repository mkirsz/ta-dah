#include "d2_dummy.h"

Registry<D2_Base,Config>::Register<D2_Dummy> D2_Dummy_1( "D2_Dummy" );

D2_Dummy::D2_Dummy() {}
D2_Dummy::D2_Dummy(Config &) {}

void D2_Dummy::calc_aed(
        const double,
        const double,
        const double,
        StDescriptors::aed_rtype ) {}
void D2_Dummy::calc_dXijdri(
        const double,
        const double,
        const double,
        const double,
        StDescriptors::fd_type &) {}
void D2_Dummy::calc_all(
        const double,
        const double,
        const double,
        const double,
        StDescriptors::aed_rtype ,
        StDescriptors::fd_type &) {}
size_t D2_Dummy::size() { return 0; }
std::string D2_Dummy::label() { return lab; }
