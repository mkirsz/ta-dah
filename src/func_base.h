#ifndef FUNC_BASE_h
#define FUNC_BASE_h
#include <iomanip>
#include <iostream>
#include <limits>
#include "models/m_base.h"
#include "utils/registry.h"
#include "st_descriptors.h"
#include <vector>

using aed_type = StDescriptors::aed_type;
using aed_rtype = StDescriptors::aed_rtype;
using aed_rctype = StDescriptors::aed_rctype;
using aeds_type = StDescriptors::aeds_type;

/** Base class for Kernels and Basis Functions */
struct Func_Base {

    // Derived classes must implement Derived() and Derived(Config)
    virtual ~Func_Base() {};

    //// For Kernels
    virtual double operator() (const aed_rctype& , const aed_rctype& ) { return 0.0; };
    virtual aed_type derivative(const aed_rctype& , const aed_rctype& ) { return aed_type(); };
    virtual double prime(const aed_rctype& , const aed_rctype& ,
            const aed_rctype& ) { return 0.0; }
    virtual void set_basis(const aeds_type ) {};

    /** \brief Return label of this object */
    virtual std::string get_label() { return "Func_Base"; };
    virtual double epredict(const t_type &, const aed_rctype& ) { return 0.0;};
    virtual double fpredict(const t_type &, const fd_type &,
            const aed_rctype&, const size_t ) {return 0.0;};
    virtual force_type fpredict(const t_type &, const fd_type &,
            const aed_rctype& ) { return force_type(); };
    virtual size_t get_phi_cols(const Config &) {return 0;};
    virtual void calc_phi_energy_row(phi_type &, size_t &,
            const double , const Structure &, const StDescriptors &){};
    virtual void calc_phi_force_rows(phi_type &, size_t &,
            const double , const Structure &, const StDescriptors &){};
    virtual void calc_phi_stress_rows(phi_type &, size_t &,
            const double[6], const Structure &, const StDescriptors &){};
};
#endif
