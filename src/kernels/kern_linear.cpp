#include "kern_linear.h"

Registry<Func_Base>::Register<Kern_Linear> Kern_Linear_1( "Kern_Linear" );
Registry<Func_Base,Config>::Register<Kern_Linear> Kern_Linear( "Kern_Linear" );

Kern_Linear::Kern_Linear() {}
Kern_Linear::Kern_Linear (const Config &c)
{
    verbose = c.get<int>("VERBOSE");
    if (verbose) std::cout << std::endl << label << std::endl;
}
double Kern_Linear::operator() (const aed_rctype& b, const aed_rctype& af)
{
    return b.transpose()*af;;
}
aed_type Kern_Linear::derivative(const aed_rctype& b, const aed_rctype&)
{
    return b;
}
double Kern_Linear::prime(const aed_rctype& b, const aed_rctype& , const aed_rctype& ff)
{
    // return derivative(b,af).transpose() * ff;
    return b.transpose() * ff;
}
std::string Kern_Linear::get_label()
{
    return label;
}
double Kern_Linear::epredict(const t_type &weights, const aed_rctype& aed)
{
    return weights.transpose()*aed;
}
double Kern_Linear::fpredict(const t_type &weights, const fd_type &fdij,
        const aed_rctype& , const size_t k)
{
    return -fdij.col(k).transpose() * weights;
}
force_type Kern_Linear::fpredict(const t_type &weights, const fd_type &fdij,
        const aed_rctype& )
{
    return -fdij.transpose() * weights;
}
size_t Kern_Linear::get_phi_cols(const Config &config)
{
    size_t cols = config.get<size_t>("DSIZE");
    return cols;
}
void Kern_Linear::calc_phi_energy_row(phi_type &Phi, size_t &row,
        const double fac, const Structure &, const StDescriptors &st_d)
{
    for (size_t a=0; a<st_d.naed();++a) {
        const StDescriptors::aed_rctype& aed = st_d.get_aed(a);
        Phi.row(row) += aed;
    }
    Phi.row(row++) *= fac;
}
void Kern_Linear::calc_phi_force_rows(phi_type &Phi, size_t &row,
        const double fac, const Structure &st, const StDescriptors &st_d)
{
    for (size_t a=0; a<st.natoms(); ++a) {
        for (size_t jj=0; jj<st_d.fd[a].size(); ++jj) {
            const size_t j=st.near_neigh_idx[a][jj];
            const size_t aa = st.get_nn_iindex(a,j,jj);
            for (size_t k=0; k<3; ++k) {
                Phi.row(row+k) -= fac*(st_d.fd[a][jj].col(k)-
                        st_d.fd[j][aa].col(k));
            }
        }
        row+=3;
    }

}
void Kern_Linear::calc_phi_stress_rows(phi_type &Phi, size_t &row,
        const double fac[6], const Structure &st, const StDescriptors &st_d)
{
    for (size_t i=0; i<st.natoms(); ++i) {
        const Eigen::Vector3d &ri = st(i).position;
        for (size_t jj=0; jj<st_d.fd[i].size(); ++jj) {
            const size_t j=st.near_neigh_idx[i][jj];
            const size_t ii = st.get_nn_iindex(i,j,jj);
            const StDescriptors::fd_type &fdij = st_d.fd[i][jj];
            const StDescriptors::fd_type &fdji = st_d.fd[j][ii];
            const Eigen::Vector3d &rj = st.nn_pos(i,jj);
            size_t mn=0;
            for (size_t x=0; x<3; ++x) {
                for (size_t y=x; y<3; ++y) {
                    Phi.row(row+mn) += 0.5*fac[mn]*(ri(x)-rj(x))*(fdij.col(y)-fdji.col(y));
                    mn++;
                }
            }
        }
    }
    row += 6;
}
