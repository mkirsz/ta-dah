#include "kern_base.h"

//template<> Registry<Kern_Base>::Map Registry<Kern_Base>::registry{};
//template<> Registry<Kern_Base,Config>::Map Registry<Kern_Base,Config>::registry{};

Kern_Base::~Kern_Base() {}
void Kern_Base::set_basis(const aeds_type b)
{
    basis = b;
}
double Kern_Base::epredict(const t_type &kweights, const aed_rctype &aed)
{
    double energy=0;
    for (long int b=0; b<basis.cols(); ++b) {
        energy += kweights[b]*(*this)(basis.col(b),aed);
    }
    return energy;
}
double Kern_Base::fpredict(const t_type &kweights, const fd_type &fdij,
        const aed_rctype &aedi, const size_t k)
{
    double res=0.0;
    for (long int b=0; b<basis.cols(); ++b) {
        res -= kweights[b]*(*this).prime(basis.col(b),aedi,fdij.col(k));
    }
    return res;
}
force_type Kern_Base::fpredict(const t_type &kweights, const fd_type &fdij,
        const aed_rctype &aedi)
{
    force_type v(3);
    v.setZero();
    for (long int b=0; b<basis.cols(); ++b) {
        for (size_t k=0; k<3; ++k) {
            v(k) -= kweights[b]*(*this).prime(basis.col(b),aedi,fdij.col(k));
        }
    }
    return v;
}
size_t Kern_Base::get_phi_cols(const Config &)
{
    return basis.size();
}
void  Kern_Base::calc_phi_energy_row(phi_type &Phi, size_t &row, const double fac,
        const Structure &, const StDescriptors &st_d)
{
    for (size_t a=0; a<st_d.naed();++a) {
        for (long int b=0; b<basis.cols(); ++b) {
            Phi(row,b) += (*this)(basis.col(b),st_d.get_aed(a));
        }
    }
    Phi.row(row++) *= fac;
}
void  Kern_Base::calc_phi_force_rows(phi_type &Phi, size_t &row, const double fac,
        const Structure &st, const StDescriptors &st_d) {
    for (size_t i=0; i<st.natoms(); ++i) {
        const StDescriptors::aed_rctype& aedi = st_d.get_aed(i);
        for (size_t jj=0; jj<st_d.fd[i].size(); ++jj) {
            size_t j=st.near_neigh_idx[i][jj];
            size_t ii = st.get_nn_iindex(i,j,jj);
            const StDescriptors::fd_type &fdji = st_d.fd[j][ii];
            const StDescriptors::fd_type &fdij = st_d.fd[i][jj];
            const StDescriptors::aed_rctype& aedj = st_d.get_aed(j);
            for (long int b=0; b<basis.cols(); ++b) {
                for (size_t k=0; k<3; ++k) {
                    Phi(row+k,b) -= fac*((*this).prime(basis.col(b), aedi,fdij.col(k)) -
                            (*this).prime(basis.col(b),aedj,fdji.col(k)));
                }
            }
        }
        row+=3;
    }
}
void  Kern_Base::calc_phi_stress_rows(phi_type &Phi, size_t &row, const double fac[6],
        const Structure &st, const StDescriptors &st_d)
{
    for (size_t i=0; i<st.natoms(); ++i) {
        const Eigen::Vector3d &ri = st(i).position;
        const StDescriptors::aed_rctype& aedi = st_d.get_aed(i);
        for (size_t jj=0; jj<st_d.fd[i].size(); ++jj) {
            size_t j=st.near_neigh_idx[i][jj];
            size_t ii = st.get_nn_iindex(i,j,jj);
            const StDescriptors::fd_type &fdji = st_d.fd[j][ii];
            const StDescriptors::fd_type &fdij = st_d.fd[i][jj];
            const StDescriptors::aed_rctype& aedj = st_d.get_aed(j);
            const Eigen::Vector3d &rj = st.nn_pos(i,jj);
            size_t mn=0;
            for (size_t x=0; x<3; ++x) {
                for (size_t y=x; y<3; ++y) {
                    for (long int b=0; b<basis.cols(); ++b) {
                        Phi(row+mn,b) += 0.5*fac[mn]*(ri(x)-rj(x))*
                            ((*this).prime(basis.col(b),aedi,fdij.col(y)) -
                             (*this).prime(basis.col(b),aedj,fdji.col(y)));
                    }
                    mn++;
                }
            }
        }
    }
    row+=6;
}
