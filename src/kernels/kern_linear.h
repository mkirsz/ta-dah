#ifndef KERN_LINEAR_H
#define KERN_LINEAR_H
#include "kern_base.h"
/**
 * Linear kernel also knows as dot product kernel
 *
 * Defined for two vectors **x** and **y**:
 *
 * \f[
 * K(\mathbf{x}, \mathbf{y}) = \mathbf{x}^T \mathbf{y} = \sum_i x^{(i)} y^{(i)}
 * \f]
 *
 *  @see Kern_Base BF_Linear
 */
class Kern_Linear :  public Kern_Base /*, public BF_Linear*/ {
    public:
    //using BF_Linear::epredict;
    //using BF_Linear::fpredict;
    //using BF_Linear::calc_phi_energy_row;
    //using BF_Linear::calc_phi_force_rows;
    //using BF_Linear::calc_phi_stress_rows;
    //using BF_Linear::get_phi_cols;

    Kern_Linear ();
    Kern_Linear (const Config &c);

    /**
     * Label used for this class
     */
    std::string label = "Kern_Linear";
    double operator() (const aed_rctype& b, const aed_rctype& af);
    aed_type derivative(const aed_rctype& b, const aed_rctype& );
    double prime(const aed_rctype& b, const aed_rctype& af, const aed_rctype& ff);
    std::string get_label();
    void set_basis(const aeds_type ) {}

    double epredict(const t_type &weights, const aed_rctype& aed);
    double fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi, const size_t k);
    force_type fpredict(const t_type &weights, const fd_type &fdij,
            const aed_rctype& aedi);
    size_t get_phi_cols(const Config &config);
    void calc_phi_energy_row(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d);
    void calc_phi_force_rows(phi_type &Phi, size_t &row,
            const double fac, const Structure &st, const StDescriptors &st_d);
    void calc_phi_stress_rows(phi_type &Phi, size_t &row,
            const double fac[6], const Structure &st, const StDescriptors &st_d);
};
#endif
