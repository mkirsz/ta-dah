#ifndef NORMALISER_H
#define NORMALISER_H

#include "libs/Eigen/Dense"
#include <limits>
#include <stdexcept>
#include "st_descriptors_db.h"
#include "config.h"

// We do calculate and store st. dev. and mean
// for the first column of Phi matrix
// for linear kernel, first column are 1s
// so we have to deal with them here
class Normaliser {
    public:
        bool bias;
        Eigen::VectorXd std_dev;
        Eigen::VectorXd mean;

        Normaliser () {};
        Normaliser (Config &c):
            bias(c.get<bool>("BIAS")),
            verbose(c.get<int>("VERBOSE"))
    {

    };
        Normaliser(Config &c,StDescriptorsDB &st_desc_db):
            bias(c.get<bool>("BIAS")),
            verbose(c.get<int>("VERBOSE"))
        {
            learn(st_desc_db);

            // normalise
            normalise(st_desc_db);
        }
        void learn(StDescriptorsDB &st_desc_db) {
            // find total number of aeds
            size_t n=0; // total number of aeds, one aed per atom
            for (size_t s=0; s<st_desc_db.size(); ++s) {
                n+= st_desc_db(s).naed();
            }

            // prep containers
            size_t dim = st_desc_db(0).dim();
            mean = Eigen::VectorXd(dim);
            std_dev = Eigen::VectorXd(dim);

            //std::cout << "COLS NORM: " << nn << std::endl;
            // compute mean and st_dev
            for (size_t d=0; d<dim; ++d) {
                Eigen::VectorXd v(n);
                size_t b=0;
                for (size_t s=0; s<st_desc_db.size(); ++s) {
                    //for (const auto &aed:st_desc_db(s).aed) {
                    for (size_t a=0; a<st_desc_db(s).naed(); ++a) {
                        v(b++) = st_desc_db(s).get_aed(a)(d);
                    }
                }
                std_dev(d) = std::sqrt((v.array() - v.mean()).square().sum()/(v.size()));
                mean(d) = v.mean();
            }


            // Filter out std dev=0 -> set them to inf
            // such that division by stdev gives 0.
            // Do it for all but bias
            size_t b=0;
            if (bias)
                b++;
            for (long int i=b; i<std_dev.size(); ++i) {
                if (std_dev[i]==0) {
                    std_dev[i] = std::numeric_limits<double>::max();
                    //std_dev[i] = std::numeric_limits<double>::infinity();
                }
            }

            if (verbose) std::cout << std::endl << "NORMALISER STDEV  : " << std_dev.transpose() << std::endl;
            if (verbose) std::cout << std::endl << "NORMALISER MEAN   :" << mean.transpose() << std::endl;

        }


        /** Normalise AED */
        void normalise(StDescriptors::aed_rtype aed) {
            size_t n=aed.size();
            if (bias)
                aed.tail(n-1) = (aed.tail(n-1) - mean.tail(n-1)).array() / std_dev.array().tail(n-1);
            else
                aed = (aed - mean).array() / std_dev.array();
        }

        /** Normalise FD */
        void normalise(StDescriptors::fd_type &fd) {
            // fd is nx3
            size_t n=fd.rows();
            if (bias) {
                for (size_t k=0; k<3; ++k)
                    fd.col(k).tail(n-1) = fd.col(k).tail(n-1).array() / std_dev.array().tail(n-1);
            }
            else {
                for (size_t k=0; k<3; ++k)
                    fd.col(k) = fd.col(k).array() / std_dev.array();
            }
        }
        void normalise(StDescriptors::fd_type &fd, size_t k) {
            // fd is nx3
            size_t n=fd.rows();
            if (bias) {
                fd.col(k).tail(n-1) = fd.col(k).tail(n-1).array() / std_dev.array().tail(n-1);
            }
            else {
                fd.col(k) = fd.col(k).array() / std_dev.array();
            }
        }

        void normalise(StDescriptors &st_d) {
            if (st_d.normalised)
                throw std::runtime_error("StDescriptors object is already normalised\n");

            //for (auto &aed: st_d.aed) normalise(aed);
            for (size_t i=0; i<st_d.naed(); ++i) normalise(st_d.get_aed(i));
            for (auto &fdv: st_d.fd)
                for (auto &fd: fdv)
                    normalise(fd);
            st_d.normalised=true;
        }

        void normalise(StDescriptorsDB &st_desc_db) {
            for (size_t s=0; s<st_desc_db.size(); ++s)
                normalise(st_desc_db(s));
        }

    private:
        int verbose;
};
#endif
