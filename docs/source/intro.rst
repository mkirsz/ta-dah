.. _introduction:

Introduction
============

Ta-dah! is a modular and fast machine learning **software** and C++ **library** specifically designed
for the interatomic potential development in mind. It is written in modern C++
with the intention to provide an easy to use, modular and extensible state-of-the-art toolkit.

Ta-dah provides LAMMPS interface which works with all provided descriptors and models.

Ta-dah! can be used as a software from the command line to train models
or use already developed machine learning potentials (with Ta-dah!) for prediction.
Alternatively, it can be included as a C++ library for the more advanced use.


Why use Ta-dah!?
----------------

 * Ta-dah! is community driven and new ideas are always welcome. 
   If it is possible to implement it into Ta-dah! it will be done.
 * Ta-dah! is fast.
   Typical model development cycle is much faster - no more waiting for hours
   to refit a model.
 * Ta-dah! is improving almost every day.
   New descriptors and models are being added. Bugs are squashed and issues are resolved.
 * Ta-dah! is open-sourced.
 * Ta-dah! allows to mix and match various descriptors with cutoffs and models.
   Once model is trained it can be immediately tested with LAMMPS. It just works!
 * Ta-dah! is extensible.
   Adding new descriptors is fairly easy and they will work seemingly with the rest
   of the code (including LAMMPS) interface.

.. _requirements:

Requirements
------------

Ta-dah! does not require any external libraries to be build or to be downloaded.

.. _downloading:

Obtaining Ta-dah!
-----------------

The code lives here:

    https://git.ecdf.ed.ac.uk/s1351949/ta-dah

To download use git and clone from the stable branch:

.. code-block:: bash

    git clone -b stable https://git.ecdf.ed.ac.uk/s1351949/ta-dah.git

.. _installation:

Installation
------------

The library uses CMake to control configuration and compilation process.
The CMake will try to guess number of system depended variables based on the config file :file:`CMakeList.txt` located in the project root directory.


.. code-block:: bash

    # cd into project dir
    cd ta-dah

    # create build directory and cd to it
    mkdir build && cd build

    # Use cmake to configure
    cmake .. 

    # compile and install 
    make && make install

To change default library installation location run below command instead of :code:`cmake ..`. This is useful when you don't have root privileges.

.. code-block:: bash

	cmake .. -DCMAKE_INSTALL_PREFIX=/your/path

Binary file
...........

The executable file :file:`ta-dah` will be installed to :file:`bin` directory inside your installation location. On most UNIX systems the default is :file:`/usr/local/bin/ta-dah`.

In case of using :code:`-DCMAKE_INSTALL_PREFIX=/your/path` flag, the binary file location is :file:`/your/path/bin/ta-dah`.

This is usually the end of the installation process for most users. If you are intending to use Ta-dah! as a C++ library continue to the next section.

Shared Library and C++ Headers
..............................

The header files and shared library will be installed to the system default location (on UNIX headers are usually installed to: :file:`/usr/local/include` and :file:`libtadah.so` shared libary to :file:`/usr/local/lib`) or the path specified by the user with :code:`-DCMAKE_INSTALL_PREFIX` flag.

When compiling code with Ta-dah! make sure that linker can find shared library object. On UNIX machines this is usually done by exporting LD_LIBRARY_PATH variable. Similarly compiler must know where header files are stored. For example, for GCC compiler this is done by exporting CPATH. This step is usually not required when library is installed to the system default location.

.. code-block:: bash
	
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/lib
	export CPATH=$CPATH:/path/to/include


.. _linking:

Compiling and Linking
---------------------

The library must be compiled with C++17 or higher.
To include library header remember to use prefix :code:`tadah/`. For example to use methods defined in :code:`atom.h`

.. code-block:: c

    #include <tadah/atom.h>

To compile code written with Ta-Dah! link :code:`ltadah`. If OpenMP is available link :code:`fopenmp`. See :ref:`gofasterstripe` for details.

For example, to compile :file:`test.cpp` with GNU :code:`g++` 

.. code-block:: bash

    g++ -O3 -std=c++17 -o output_file test.cpp -ltadah -fopenmp


If the library is not installed in the standard search path you will have to link it manually, for example:

.. code-block:: bash

    g++ -O3 -std=c++17 -o output_file test.cpp -I/home/$USER/.local/include -L/home/$USER/.local/lib -ltadah -fopenmp

.. _gofasterstripe:

Go Faster Stripe
----------------

The library will be compiled with OpenMP if it is available on the system. OpenMP uses the system variable to control the number of threads.

To set number of threads to be used by the library on a UNIX machine:

.. code-block:: bash
	
	export OMP_NUM_THREADS=num_of_cores

Always link :code:`fopenmp` when compiling code which uses Ta-dah! library.


.. _installation_lammps:

Installing LAMMPS interface
---------------------------

First, install Ta-dah! library as described in :ref:`installation`.

To build Ta-dah with lammps you need to download LAMMPS source code if you don't have it.

Copy :file:`tadah/build/USER-TADAH` directory to LAMMPS :file:`src` directory.

.. warning:: Make sure you are copying :file:`USER-TADAH` from the build directory and NOT from the :file:`src/lammps/USER-TADAH`.

.. hint:: The file :file:`USER-TADAH/Makefile.lammps` specifies paths to :code:`libtadah`.
          If it is not installed in the default location you might have to edit this file. Cmake will try to guess correct locations.

Go to lammps source directory (e.g. :file:`mylammps/src`) and run :code:`make yes-user-tadah` and then recompile LAMMPS (e.g. :code:`make serial`).

.. important:: Ta-dah should work with LAMMPS versions from June 2022 onwards.
               It has been tested with the following LAMMPS version: 23 Jun 2022 - Update 2.

.. seealso:: Detailed instructions on how to build LAMMPS with extra features: 

             https://lammps.sandia.gov/doc/Build_package.html


.. _troubleshooting:

Troubleshooting
---------------

If you have any issues with a compilation process do not hesitate to drop me an email.

#. Issue: **Permission denied**

   This error might happen when running :code:`make install`.
   
   ::

     Install the project...
     -- Install configuration: "Release"
     -- Installing: /usr/local/bin/ta-dah
     CMake Error at src/cmake_install.cmake:55 (file):
       file INSTALL cannot copy file
       "/home/$USER/ta-dah/build/src/ta-dah" to
       "/usr/local/bin/ta-dah": Permission denied.
     Call Stack (most recent call first):
       cmake_install.cmake:42 (include)


     make: *** [Makefile:129: install] Error 1

   You are installing to root directory without root privileges hence
   files cannot be copied to the required destination.
   Either run the command with root privileges (**sudo** or equivalent)
   or change install location to user directory (e.g. :file:`$HOME/.local`
   for most UNIX systems):

   .. code-block:: bash

     cmake .. -DCMAKE_INSTALL_PREFIX=~/.local
     make install

#. Issue: **Shared object ImportError**

   .. code-block:: bash

      ImportError: libtadah.so: cannot open shared object file: No such file or directory
  
   libtadah is a dynamic library and your system does not know where to find it.
   Adding the directory containing libtadah.so to LD_LIBRARY_PATH should solve the problem.

   .. code-block:: bash

      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/your/path/

   It usually make sense to add above command to '~/.bashrc' file so it is executed each time a new terminal window is opened.

#. Issue: **error: ‘...class LAMMPS_NS::Comm’ has no member named... or similar**

    ::
    
     ../pair_tadah_tadah.cpp: In member function ‘void LAMMPS_NS::PairTadah::compute_2b_mb_half(int, int)’:
     ../pair_tadah_tadah.cpp:242:15: error: ‘class LAMMPS_NS::Comm’ has no member named ‘reverse_comm_pair’; did you mean   ‘reverse_comm’?
     242          comm->reverse_comm_pair(this);
     ../pair_tadah_tadah.cpp:263:15: error: ‘class LAMMPS_NS::Comm’ has no member named ‘forward_comm_pair’; did you mean     ‘forward_comm_array’?
     263          comm->forward_comm_pair(this);

   This is likely to be an incopatibility between your version of LAMMPS and Ta-dah!
   See :ref:`installation_lammps` for more information.       


#. Issue: **error: ‘...error: ‘int LAMMPS_NS::NeighRequest::half’ is protected within this context... or similar**

    ::

     ../pair_tadah_tadah.cpp: In member function ‘virtual void LAMMPS_NS::PairTadah::init_style()’:
     ../pair_tadah_tadah.cpp:888:43: error: ‘int LAMMPS_NS::NeighRequest::half’ is protected within this context
     ../pair_tadah_tadah.cpp:263:15: error: ‘class LAMMPS_NS::Comm’ has no member named ‘forward_comm_pair’; did you mean ‘forward_comm_array’?

   This is likely to be an incopatibility between your version of LAMMPS and Ta-dah!
   See :ref:`installation_lammps` for more information.       


#. Issue: **New issue**

   **If you have an issue which is not listed here please get in touch.**
