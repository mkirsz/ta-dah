Basis Functions
===============

BF_Linear
.........

.. doxygenstruct:: BF_Linear
    :members:

BF_Polynomial2
..............

.. doxygenstruct:: BF_Polynomial2
    :members:

BF_Base
.......

.. doxygenstruct:: BF_Base
    :members:
