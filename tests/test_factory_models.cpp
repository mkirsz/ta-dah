#include "catch2/catch.hpp"
#include <limits>
#include <stdexcept>
#include "../src/utils/registry.h"
#include "../src/cutoffs/cutoffs.h"
#include "../src/descriptors/d_all.h"
#include "../src/models/m_all.h"
#include "../src/structure_db.h"
#include "../src/st_descriptors_db.h"
#include "../src/config.h"
#include "../src/nn_finder.h"
#include <string>

    using BF=BF_Linear;
    using K=Kern_Linear;
    using D2=D2_Dummy;
    using D3=D3_Dummy;
    using DM=DM_Dummy;
    using C2=Cut_Dummy;
    using C3=Cut_Dummy;
    using CM=Cut_Dummy;


TEST_CASE( "Testing Factory: Models", "[factory_models]" ) {

    Config config;
    config.add("MPARAMS",1);
    config.add("MPARAMS",2);
    config.add("MPARAMS",3);
    // Config contains defaults only, we want to run prediction
    // should compile and run
    // should throw runtime b/c e.g. WEIGHTS key is not found
    for (auto b:Registry<Func_Base,Config>::registry) {
        Func_Base *fb = factory<Func_Base,Config>(b.first,config);
        for (auto c:Registry<M_Base,Func_Base,Config>::registry) {
            if (dynamic_cast<Kern_Base*>(fb) != nullptr && c.first=="M_KRR") {
                //REQUIRE_THROWS(factory<M_Base,Func_Base,Config>(c.first,*fb,config));
                //REQUIRE_THROWS_AS((factory<M_Base,Func_Base,Config>(c.first,*fb,config)),std::runtime_error);
                //REQUIRE_THROWS_WITH((factory<M_Base,Func_Base,Config>(c.first,*fb,config)), Catch::Matchers::Contains( "Key not found" ));
            }
            else if (dynamic_cast<BF_Base*>(fb) != nullptr && c.first=="M_BLR") {
                //REQUIRE_THROWS(factory<M_Base,Func_Base,Config>(c.first,*fb,config));
                //REQUIRE_THROWS_AS((factory<M_Base,Func_Base,Config>(c.first,*fb,config)),std::runtime_error);
                //REQUIRE_THROWS_WITH((factory<M_Base,Func_Base,Config>(c.first,*fb,config)), Catch::Matchers::Contains( "Key not found" ));
            }
            else {
                //REQUIRE_THROWS_AS((factory<M_Base,Func_Base,Config>(c.first,*fb,config)),
                //        std::invalid_argument);
            }
        }
    }

    //config = Config("datasets/config");
    //StructureDB stdb(config);
    //NNFinder nnf(config);
    //nnf.calc(stdb);
    //D2 d2b(config);
    //D3 d3b(config);
    //DM dmb(config);

    //Cut_Base *c2b;
    //Cut_Base *c3b;
    //Cut_Base *cmb;
    //double rcut2b = config.get<double>("RCUT2B");
    //double rcut3b = config.get<double>("RCUT3B");
    //double rcutmb = config.get<double>("RCUTMB");
    //for (auto c:Registry<Cut_Base,double>::registry) {
    //    c2b = factory<Cut_Base,double>( c.first, rcut2b );
    //    c3b = factory<Cut_Base,double>( c.first, rcut3b );
    //    cmb = factory<Cut_Base,double>( c.first, rcutmb );
    //}
    //Func_Base *fb = factory<Func_Base,Config>(config.get<std::string>("MODEL",1),config);
    //for (auto m:Registry<M_Base,Func_Base,D2_Base,D3_Base,DM_Base,Cut_Base,Cut_Base,Cut_Base,Config,StructureDB>::registry) {
    //}
    //M_Base *model = factory<M_Base,Func_Base,D2_Base,D3_Base,DM_Base,
    //      Cut_Base,Cut_Base,Cut_Base,Config,StructureDB>
    //          (config.get<std::string>("MODEL",0),bf,d2b,d3b,dmb,
    //           c2b,c3b,cmb,config,stdb);

}

TEST_CASE( "Testing M_BLR", "[m_blr]" ) {

    Config config;
    // Config contains defaults only, we run prediction
    // should throw runtime b/c key is not found
    //using M1=M_BLR<BF,D2,D3,DM,C2,C3,CM>;
    //REQUIRE_THROWS_AS(M1(config),std::runtime_error);
    //REQUIRE_THROWS_WITH(M1(config), Catch::Matchers::Contains( "Key not found" ));
    // for later...
    D2 d2(config);
    D3 d3(config);
    DM dm(config);
    C2 c2(0.0);
    C3 c3(0.0);
    CM cm(0.0);

}

TEST_CASE( "Testing M_KRR", "[m_krr]" ) {

    Config config;
    // Config contains defaults only, we want to run prediction
    // should throw runtime b/c e.g. WEIGHTS key is not found
    //using M2=M_KRR<K>;
    //REQUIRE_THROWS_AS(M2(config),std::runtime_error);
    //REQUIRE_THROWS_WITH(M2(config), Catch::Matchers::Contains( "Key not found" ));
    // for later...
    D2 d2(config);
    D3 d3(config);
    DM dm(config);
    C2 c2(0.0);
    C3 c3(0.0);
    CM cm(0.0);

}

