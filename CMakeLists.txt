cmake_minimum_required(VERSION 3.12)

# There is usually no need to change anything here...


set(TADAH "Ta-dah!")
file(READ "src/version.h" ver)
string(REGEX MATCH "TADAH_WORLD_VERSION ([0-9])" _ "${ver}")
set(ver_world ${CMAKE_MATCH_1})

string(REGEX MATCH "TADAH_MAJOR_VERSION ([0-9])" _ "${ver}")
set(ver_major ${CMAKE_MATCH_1})

string(REGEX MATCH "TADAH_MINOR_VERSION ([0-9])" _ "${ver}")
set(ver_minor ${CMAKE_MATCH_1})

project(Ta-dah VERSION ${ver_world}.${ver_major}.${ver_minor})
message(STATUS "${TADAH} VERSION: ${ver_world}.${ver_major}.${ver_minor}")

# Here are the options... set them with -D
option(TADAH_ENABLE_HPO "Enable Hyperparamter optimiser." OFF)
option(TADAH_ENABLE_OPENMP "Enable shared-memory parallelisation via OpenMP" OFF)
option(TADAH_ENABLE_MPI "Enable parallelisation via MPI" OFF)
option(TADAH_MARCH_NATIVE "Build with -march=native" OFF)
find_package(Doxygen)
option(TADAH_BUILD_DOCUMENTATION "Create and install the HTML based        
documentation (requires Doxygen)" ${DOXYGEN_FOUND})
option(TADAH_ENABLE_FAST_MATH "Enable fast math optimizations" OFF)


# This option is experimental. Static build is only partially supported
# and the result depends on the components selected.
# In fact, for now it only works with vanilla tadah
# 
# Do not build as static if MPI or OpenMPI are used.
option(TADAH_BUILD_SHARED_LIBS "Build ${TADAH} as shared library (defaul)" ON)
# perhaps add if mpi and static then fail... and so on

# Here we only need name so for e.g. liblammps_serial.so use lammps_serial
if(NOT DEFINED TADAH_LAMMPS_LIB)
    set(TADAH_LAMMPS_LIB "lammps_serial")
endif()

# guard against in-source builds
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there. You may need to remove CMakeCache.txt and CMakeFiles dir. ")
  endif()

if(UNIX AND NOT APPLE)
endif()

if(APPLE)
endif()

if(WIN32)
endif()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


if (TADAH_BUILD_SHARED_LIBS)
    set(CMAKE_POSITION_INDEPENDENT_CODE ON)
    set(_TADAH_LIB_BUILD SHARED)
else()
    set(_TADAH_LIB_BUILD STATIC)
    #set(CMAKE_EXE_LINKER_FLAGS "-static")
    set(BUILD_SHARED_LIBS OFF)
endif()


if (TADAH_BUILD_SHARED_LIBS)
    # Always full RPATH
    # use, i.e. don't skip the full RPATH for the build tree
    set(CMAKE_SKIP_BUILD_RPATH FALSE)

    # when building, don't use the install RPATH already
    # (but later on when installing)
    set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

    # add the automatically determined parts of the RPATH
    # which point to directories outside the build tree to the install RPATH
    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

    # the RPATH to be used when installing, but only if it's not a system directory
    list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
    if("${isSystemDir}" STREQUAL "-1")
        set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
    endif("${isSystemDir}" STREQUAL "-1")
endif()


if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS_DEBUG "-g -O2 -fsanitize=address -Wall -Wpedantic -Wextra")
set(CMAKE_CXX_FLAGS_RELEASE "-g -O3 -Wall -Wpedantic -Wextra")

message(STATUS "${TADAH} Build with -march=native in ${TADAH_MARCH_NATIVE}")
if(TADAH_MARCH_NATIVE)
    include(CheckCXXCompilerFlag)
    CHECK_CXX_COMPILER_FLAG("-march=native" COMPILER_SUPPORTS_MARCH_NATIVE)
    if(COMPILER_SUPPORTS_MARCH_NATIVE)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native")
    else()
        message(STATUS "${TADAH} ...but -march=native is not supported")
    endif()
endif()

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)


if(TADAH_BUILD_DOCUMENTATION)
	if(NOT DOXYGEN_FOUND)
		message(FATAL_ERROR "Doxygen is needed to build the documentation.")
	endif()

	set(doxyfile_in ${CMAKE_CURRENT_SOURCE_DIR}/docs/Doxyfile)
	set(doxyfile ${CMAKE_CURRENT_BINARY_DIR}/doxyfile)

	configure_file(${doxyfile_in} ${doxyfile} @ONLY)

	add_custom_target(
		docs
		COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile_in}
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/docs
		COMMAND make html
		COMMENT "Generating API documentation with Doxygen"
		VERBATIM
		)
endif()

if(TADAH_ENABLE_FAST_MATH)
    message(STATUS "${TADAH} Build with fast math optimizations")
	if((CMAKE_CXX_COMPILER_ID MATCHES "Clang") OR (CMAKE_CXX_COMPILER_ID MATCHES "GNU"))
		add_compile_options(-ffast-math)
	elseif(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
		add_compile_options(/fp:fast)
	endif()
else()
	if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
		add_compile_options(/fp:precise)
	endif()
endif()

add_subdirectory(src)

file(COPY src/lammps/USER-TADAH DESTINATION .)

include(CTest) 
if(TADAH_BUILD_TESTING)
    add_subdirectory(tests)
endif()

set(CATCH_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/tests/catch2)


configure_file(src/lammps/USER-TADAH/Makefile.lammps USER-TADAH/Makefile.lammps)

message(STATUS "===================================================================")
message(STATUS "                                                                   ")
message(STATUS "${TADAH} in now configured and ready for installation.             ")
message(STATUS "                                                                   ")
message(STATUS "--------------+----------------------------------------------------")
message(STATUS "Command       |   Description                                      ")
message(STATUS "--------------+----------------------------------------------------")
message(STATUS "              |                                                    ")
message(STATUS "make install  |   Install ${TADAH}                                 ")
message(STATUS "              |                                                    ")
message(STATUS "              |   Executable file will installed to:               ")
message(STATUS "              |      ${CMAKE_INSTALL_PREFIX}/bin                   ")
message(STATUS "              |   Headers will be installed to:                    ")
message(STATUS "              |      ${CMAKE_INSTALL_PREFIX}/include/tadah         ")
message(STATUS "              |   Shared library will be installed to:             ")
message(STATUS "              |      ${CMAKE_INSTALL_PREFIX}/lib                   ")
message(STATUS "              |                                                    ")
message(STATUS "              |   To change default install location run:          ")
message(STATUS "              |     cmake .. -DCMAKE_INSTALL_PREFIX=yourpath       ")
message(STATUS "              |                                                    ")
message(STATUS "make docs     |   Generate a local copy of html documentation:     ")
message(STATUS "              |                                                    ")
message(STATUS "              |   doxygen style:                                   ")
message(STATUS "              |      ${CMAKE_SOURCE_DIR}/docs/html/index.html      ")
message(STATUS "              |   sphinx style:                                    ")
message(STATUS "              |      ${CMAKE_SOURCE_DIR}/docs/build/html/index.html")
message(STATUS "              |                                                    ")
message(STATUS "make test     |   Run testing framework                            ")
message(STATUS "              |                                                    ")
message(STATUS "--------------+----------------------------------------------------")
message(STATUS "              OPTIONAL FEATURES:                                   ")
message(STATUS "--------------+----------------------------------------------------")
message(STATUS "                                                                   ")
message(STATUS "            * Build with Hyperparamter optimiser.                  ")
message(STATUS "              TADAH_ENABLE_HPO is ${TADAH_ENABLE_HPO}              ")
if(TADAH_ENABLE_HPO)
message(STATUS "              Requires LAMMPS to be compiled as shared serial lib  ")
message(STATUS "              Linking to serial LAMMPS: ${TADAH_LAMMPS_LIB}              ")
message(STATUS "              To set LAMMPS lib path to different value use:       ")
message(STATUS "              -DTADAH_LAMMPS_LIB=lammps_serial                           ")
message(STATUS "              which corresponds to liblammps_serial                ")
message(STATUS "              To disable this option:                              ")
message(STATUS "                cmake .. -DTADAH_ENABLE_HPO=OFF                    ")
else()
message(STATUS "              To enable this option:                               ")
message(STATUS "                cmake .. -DTADAH_ENABLE_HPO=ON                     ")
endif()
message(STATUS "                                                                   ")
message(STATUS "===================================================================")
